 "use strict";
var LinkedList = require('./LinkedList');
class UtilMath {
    constructor() {
    }

    static mean(list) {
        if (!list) {
            throw 'Error empty list'
        }
        var len = list.length;
        if (len === 0) {
            return 0;
        }
        var sum = this.sum(list);
        var obj = Object.keys(list.getValue(0));
        var meanList = this.sum(list)

        for(var i = 0;i<obj.length;i++){ //se toman todos lo valores sumados de cada tipo en el object payload de la lista y se divide sobre la longitud
            sum[obj[i]] = sum[obj[i]]/list.length
        }
        return sum;
    }
    
    static sum(list) { // metodo par arealizar la sumatoria
        if (!list) {
            throw 'Error empty list'
        }
        var len = list.length;
        if (len === 0) {
            return 0;
        }
        var i, j, sum = {};
        var total = 0;
        var count,obj;
        obj = Object.keys(list.getValue(0));
        count = obj.length;
        for (j = 0; j < count; j++) {
            for (i = 0; i < len; i++) {
                if(!sum[obj[j]]){
                    sum[obj[j]] = 0;
                }
                if(!isNaN(list.getValue(i)[obj[j]])){
                    sum[obj[j]] += parseFloat(list.getValue(i)[obj[j]]);
                }
            }
        }
        return sum;
    }
    
    
    static sumSquared(list) {
        if (!list) {
            throw 'Error empty list'
        }
        var len = list.length;
        if (len === 0) {
            return 0;
        }
        var i, j, obj, count, square = {}
        obj = Object.keys(list.getValue(0));
        count = obj.length;
        for (j = 0; j < count; j++) {
            for (i = 0; i < len; i++) {
                if(!square[obj[j]]){
                    square[obj[j]] = 0;
                }
                square[obj[j]] += Math.pow(parseFloat(list.getValue(i)[obj[j]]),2);
            }
        }
        return square;
    }
    
    static sumProduct(list) {
        if (!list) {
            throw 'Error empty list'
        }
        var len = list.length;
        if (len === 0) {
            return 0;
        }
        var i, j, product = 0;
        var count, obj;
        obj = Object.keys(list.getValue(0)); // se asume que todos los elementos de la lista tengan la misma estructura
        count = obj.length;
        for (i = 0; i < len; i++) {
            for (j = 0; j < count; j += 2) {
                product += (parseFloat(list.getValue(i)[obj[j]]) * (parseFloat(list.getValue(i)[obj[j+1]])));
            }
            
        }
        return product;
    }

    static stdDeviation(list) {
        if (list.length === 0)
            return 0;
        var mean = UtilMath.mean(list)
        var len = list.length;
        var i = 0;
        var deviation = {};
        var obj = Object.keys(list.getValue(0));// se asume que todos los elementos de la lista tengan la misma estructura
        var count = obj.length; // el numero de llaves diferente itemOne itemTwo
        for (i = 0; i < len; i++) {
            for (var j = 0; j < count; j++) {
                if(!deviation[obj[j]]){
                    deviation[obj[j]] = 0;
                }
                deviation[obj[j]] += Math.pow((parseFloat(list.getValue(i)[obj[j]]) - mean[obj[j]]), 2);
            }
        }
        for (var j = 0; j < count; j++) {
            deviation[obj[j]] = parseFloat(deviation[obj[j]] / (len - 1));
            deviation[obj[j]] = parseFloat(Math.sqrt(deviation[obj[j]]));
        }
        return deviation;
    }

    static betaOne(list) {
        if (list.length == 0)
            return 0;
        var sum = UtilMath.sum(list)
        var mean = UtilMath.mean(list) 
        var square = UtilMath.sumSquared(list)
        var product = UtilMath.sumProduct(list); 
        var len = list.length;
        var betaOne = ((product - (parseFloat(len) * ( mean.itemOne * mean.itemTwo)))/((square.itemOne - (len * Math.pow(mean.itemOne, 2)))));
        return betaOne;
    }
    
    static betaZero(list) {
        if (list.length == 0)
            return 0;
        var betaOne = UtilMath.betaOne(list)
        var mean = UtilMath.mean(list) 
        var betaZero = (mean.itemTwo - (betaOne * mean.itemOne)) 
        return betaZero;
    }
    
    static correlation(list) {
        if (list.length == 0)
            return 0;
        var sum = UtilMath.sum(list)
        var mean = UtilMath.mean(list) 
        var square = UtilMath.sumSquared(list)
        var product = UtilMath.sumProduct(list); 
        var len = list.length;
        var correlationNum = ((len * product) - (sum.itemOne * sum.itemTwo));
        var correlationDen = Math.sqrt((len * (square.itemOne) - (Math.pow(sum.itemOne, 2))) * (len * (square.itemTwo) - (Math.pow(sum.itemTwo, 2))));
        var correlation = correlationNum/correlationDen;
        return correlation;
    }
    
    static rSquared(list) {
        var r = UtilMath.correlation(list)
        return Math.pow(r, 2);
    }
    
    static prediction(list) {
        var betaOne = UtilMath.betaOne(list)
        var betaZero = UtilMath.betaZero(list)
        return (betaZero + (betaOne * 386));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////PROGRAMA 3//////////////////////////////////////////////////////////////
    
    static lnArray(list){
        if (list.length === 0)
            return 0;
        if(typeof(list) !== 'object'){
            throw 'list must be an array';
        }
        var obj = Object.keys(list.getValue(0));// se asume que todos los elementos de la lista tengan la misma estructura
        var lista = new LinkedList()
        for (var i = 0; i < list.length; i++) {
            for (var j = 0; j < obj.length; j++) {
                if(!isNaN(list.getValue(i)[obj[j]])){ // si valida si e sun numero
                    lista.push({'val':Math.log(list.getValue(i)[obj[j]])}) // se ingresa
                }
            }
        }
        return(lista)
    }

    static avg(list){ //recibe un una lista que tenga un solo parametro en hash del object
        if (list.length === 0)
            return 0;
        let total = 0;
        if(typeof(list) !== 'object'){
            throw 'list must be an array';
        }
        total = UtilMath.sum(list)
        let obj = Object.keys(list.getValue(0))
        return total[obj[0]]/list.length;
        
    }
    static variance(list,avg){
        if (list.length === 0)
            return 0;
        var num, len = 0, total = 0;
        var obj = Object.keys(list.getValue(0));// se asume que todos los elementos de la lista tengan la misma estructura
        var lista = new LinkedList()
        for (var i = 0; i < list.length; i++) {
            total += Math.pow(list.getValue(i)[obj[0]]-avg,2); // solo se trabaja con un hash de un valor
            len++;
        }
        if(list.length == 0 || len == 0){
            throw 'List length must be bigger than 1';
        }
        return total/(len-1);
    }

    static standardDeviation(variance){
        if(isNaN(variance)){
            throw 'variance must be a number';
        }
        if(variance < 0){
            throw 'variance must be a positive number';
        }
        return Math.sqrt(variance)
    }

}
module.exports = UtilMath;
